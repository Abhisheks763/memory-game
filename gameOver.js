import { sendTempMsg } from "./messages.js";

function gameOver(player, noOfcardsFlipped) {
  let topScore = JSON.parse(localStorage.getItem("topScore"));

  let setScore = new Promise((resolve, reject) => {
    if (topScore == null || topScore.score > noOfcardsFlipped) {
      localStorage.setItem(
        "topScore",
        JSON.stringify({ player: player, score: noOfcardsFlipped })
      );
      resolve();
    } else {
      reject();
    }
  });

  let currentTopScore = JSON.parse(localStorage.getItem("topScore"));

  setScore
    .then((res) => {
      sendTempMsg(`🎊 Congratulations 🎊 you are the new high scorer`);
    })
    .catch((res) => {
      sendTempMsg(
        `Game Over you scored ${noOfcardsFlipped}.
      \nTop scorer is ${currentTopScore.player} with a score of ${currentTopScore.score}`
      );
    });
}

export { gameOver };
