function showTopScorer() {
  let currentTopScore = JSON.parse(localStorage.getItem("topScore"));
  document.querySelector(
    "#top-score"
  ).innerText = `Top scorer : ${currentTopScore.player}
   with a score of ${currentTopScore.score}`;
}
export { showTopScorer };
