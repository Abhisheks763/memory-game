function sendTempMsg(msg) {
  document.querySelector("#message-content p").innerText = msg;
  document.querySelector("#messages").style.display = "block";
  window.addEventListener("click", (event) => {
    if (event.target == document.querySelector("#messages")) {
      document.querySelector("#messages").style.display = "none";
    }
  });
}

export { sendTempMsg };
