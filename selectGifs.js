import { shuffle } from "./script.js";
function selectGifs(noOfCardsToDeal) {
  let gifArray = [];

  for (let i = 0; i < noOfCardsToDeal / 2; i++) {
    let gifNumber = Math.floor(Math.random() * 12) + 1;

    let gif = `./gifs/${gifNumber}.gif`;
    gifArray.push(gif);
  }

  return gifArray;
}

export { selectGifs };
