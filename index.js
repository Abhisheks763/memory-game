document.getElementById("submit").addEventListener("click", getPlayerName);

function getPlayerName() {
  playerName = document.getElementById("playerName").value;
  localStorage.setItem("playerName", playerName);
  location.assign("./game.html");
}
