import { currentScore } from "./currentScore.js";
import { sendTempMsg } from "./messages.js";
import { gameOver } from "./gameOver.js";
import { showTopScorer } from "./topScorer.js";
import { noOfcards } from "./form.js";
import { selectGifs } from "./selectGifs.js";

let playerName = localStorage.getItem("playerName");

const gameContainer = document.getElementById("game");
let noOfCardsToDeal;

let GIFS = [];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledGifs = shuffle(GIFS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForGifs(gifsArray) {
  for (let gif of gifsArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(gif);
    newDiv.backgroundImage = `url(./double-bubble-dark.png) `;

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let clickedCardsArray = [];

let gameStarted = false;

//Score at start
let noOfcardsFlipped = 0;
currentScore(noOfcardsFlipped);

// Click handler
function handleCardClick(event) {
  if (
    event.target.style.background == "" &&
    clickedCardsArray.length < 2 &&
    !clickedCardsArray.includes(event.target)
  ) {
    event.target.style.backgroundImage = `url(${event.target.classList[0]})`;
    event.target.style.backgroundSize = `100% 100%`;

    clickedCardsArray.push(event.target);

    // Updating score
    noOfcardsFlipped += 1;
    currentScore(noOfcardsFlipped);

    if (clickedCardsArray.length > 1) {
      if (
        clickedCardsArray[0].style.backgroundImage ==
        clickedCardsArray[1].style.backgroundImage
      ) {
        sendTempMsg(`It's a match`);
        clickedCardsArray = [];

        //Declaring game over
        let allDivs = document.querySelectorAll("#game div");
        let colouredDiv = [...allDivs].filter((div) => {
          return div.style.backgroundImage !== "";
        });

        if (colouredDiv.length == parseInt(noOfCardsToDeal)) {
          gameOver(playerName, noOfcardsFlipped);
          showTopScorer();
        }
      } else {
        setTimeout(
          () => {
            for (let card of clickedCardsArray) {
              card.style.backgroundImage = "url(./double-bubble-dark.png)";
            }
            clickedCardsArray = [];
          },
          1000,
          clickedCardsArray
        );
      }
    }
  }
}

//Show top score
if (localStorage.getItem("topScore") !== null) {
  showTopScorer();
}

// Reset button
document.getElementById("reset").addEventListener("click", () => {
  let allDivs = document.querySelectorAll("#game div");

  [...allDivs].forEach((div) => {
    div.remove();
  });

  GIFS = selectGifs(noOfCardsToDeal);
  GIFS = GIFS.reduce(function (res, current, index, array) {
    return res.concat([current, current]);
  }, []);
  shuffledGifs = shuffle(GIFS);

  createDivsForGifs(shuffledGifs);
  noOfcardsFlipped = 0;
  document.querySelector(
    "#score"
  ).innerText = `Your score : ${noOfcardsFlipped}`;
  clickedCardsArray = [];
  sendTempMsg("Game reset successful");
});

// when the DOM loads
// Start button

document.querySelector("#start").addEventListener("click", () => {
  noOfCardsToDeal = noOfcards();

  if (parseInt(noOfCardsToDeal) % 2 !== 0) {
    noOfCardsToDeal = 1 + parseInt(noOfCardsToDeal);
  }

  GIFS = selectGifs(noOfCardsToDeal);

  GIFS = GIFS.reduce(function (res, current, index, array) {
    return res.concat([current, current]);
  }, []);
  shuffledGifs = shuffle(GIFS);

  if (gameStarted == false && document.readyState == "complete") {
    createDivsForGifs(shuffledGifs);
    gameStarted = true;
    document.querySelector("#start").style.display = "none";
    document.querySelector("#title-div form").style.display = "none";
    document.querySelector("#reset").style.display = "inline-block";
    sendTempMsg(`Game started with ${playerName}`);
  } else {
    sendTempMsg("Game already started or document is still loading");
  }
});

export { shuffle };
